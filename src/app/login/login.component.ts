import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators } from "@angular/forms";
import { AccountService } from "../core/services/account/account.service";

import { TokenService } from "../core/services/account/token.service"


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  signForm: FormGroup
  person: any = {}
  loading: boolean = false

  constructor( private formBuilder: FormBuilder,
               private accountService: AccountService,
               private tokenService: TokenService
  ) { }

  ngOnInit() {
    this.createForm();

    if(this.tokenService.getToken()){
      this.accountService.redirectUserLogged()
    }
  }

  createForm(){
    this.signForm = this.formBuilder.group({
      email: [null, [Validators.required,Validators.email, Validators.maxLength(200)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(72)]],
    });
  }

  submitLogin(){
    this.person.username = this.signForm.get('email').value;
    this.person.password = this.signForm.get('password').value;

    this.loading = true

    this.accountService.login(this.person.username, this.person.password).subscribe((res: any) => {
      this.loading = false
            console.log('RESPONSE: ', res);
            this.tokenService.setToken(res.body)
            this.accountService.setUser(res.body.userId)

        }, error => {
          this.loading = false
          console.log('ERROR :', error)
        }
      )

  }



}
