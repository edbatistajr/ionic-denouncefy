import { Injectable } from '@angular/core';

const KEY = 'authTokenDenouncefy';
const ID = 'authUserSession';
const USER = 'authUserManagerSession';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  setToken(token){
    localStorage.setItem(KEY, token.id)
    localStorage.setItem(ID, token.userId)
  }

  setUser(user){
    localStorage.setItem(USER, JSON.stringify(user))
  }

  getToken(){
    return localStorage.getItem(KEY)
  }

  removeToken(){
    return localStorage.removeItem(KEY)
  }
  removeIdUser(){
    return localStorage.removeItem(ID)
  }


}
