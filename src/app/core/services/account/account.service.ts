import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {environment} from "../../../../environments/environment.prod";
import { TokenService } from "./token.service";


@Injectable({
  providedIn: 'root'
})

export class AccountService {

  uri = environment.URI_API;
  public user;

  constructor( private http: HttpClient,
               private router: Router,
               private tokenService: TokenService
  ) {}

  login(email: string, password: string){
    return this.http.post(this.uri + '/Accounts/login', { email: email, password: password }, { observe: 'response' })
  }


  setUser(id){
    this.http.get(this.uri + '/Accounts/' + id + '?access_token=' + this.tokenService.getToken()) .subscribe(
        (res) => {
          this.tokenService.setUser(res)
          this.user = res
          console.log('res: ', res)

          this.router.navigate(['dashboard/dashboard-home'])
        }
    )
  }


   redirectUserLogged(){
      this.router.navigate(['dashboard/dashboard-home']);
   }

    redirectNoLogged(){
        this.router.navigate(['login']);
    }

}
