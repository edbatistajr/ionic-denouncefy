import { Component, OnInit } from '@angular/core';
import {TokenService} from "../../core/services/account/token.service";
import {AccountService} from "../../core/services/account/account.service";

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss'],
})
export class DashboardHomeComponent implements OnInit {

  loading: boolean = false

  constructor(
      private tokenService: TokenService,
      private accounteService: AccountService
  ) { }

  ngOnInit() {}

  logout(){
    this.loading = true
    this.tokenService.removeIdUser()
    this.tokenService.removeToken()

    if (!this.tokenService.getToken()) {
        this.loading = false
        this.accounteService.redirectNoLogged();
    }
  }

}
